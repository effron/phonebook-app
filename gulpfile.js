var gulp          = require('gulp');
var notify        = require('gulp-notify');
var buffer 		  = require('vinyl-buffer');
var source        = require('vinyl-source-stream');
var browserify    = require('browserify');
var babelify      = require('babelify');
//var ngAnnotate    = require('browserify-ngannotate');
var browserSync   = require('browser-sync').create();
var rename        = require('gulp-rename');
var templateCache = require('gulp-angular-templatecache');
var uglify        = require('gulp-uglify');
var compass		  = require('gulp-compass');
var cleanCSS	  = require('gulp-clean-css');
var merge         = require('merge-stream');
var size		  = require('gulp-size');
var nodemon		  = require('nodemon');
const sourcemaps  = require('gulp-sourcemaps');


// Where our files are located
var clientSourcePath = "./src/client/";
var jsFiles   = clientSourcePath+"**/*.js";
var webFonts = 'src/client/assets/fonts/**/*.{eot,svg,ttf,woff}';
var imageFiles = 'src/client/assets/images/**/*.{jpg,png,gif,iff}';
var viewFiles = [
	clientSourcePath+"**/*.html"
];
var cssFiles = [clientSourcePath+'assets/sass/*.scss', clientSourcePath+'assets/sass/**/*.scss', '!'+clientSourcePath+'assets/sass/lib/'];

var buildFiles = './bin/client/';

var interceptErrors = function(error) {
  var args = Array.prototype.slice.call(arguments);

  // Send error to notification center with gulp-notify
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);
  console.log('Compile Error', error.message);

  // Keep gulp from hanging on this task
  this.emit('end');
};

gulp.task('css', () => {
	var sassFiles = clientSourcePath+'assets/sass/';
	console.log(sassFiles);
	return gulp.src(cssFiles).pipe(compass({
		//config_file: './config.rb',
		css: buildFiles,
		sass: sassFiles,
		output_style: 'nested'
	}))
	// If add on error this will keep compiling without stop on issue
 	.on('error', function(error){
		console.log(error.message);
	//   console.log(error);
		this.emit('end');
 	});
});

gulp.task('assets', function() {
	var fonts = gulp.src(webFonts)
    .pipe(gulp.dest(buildFiles+'assets/fonts/'));

	var images = gulp.src(imageFiles)
	.pipe(gulp.dest(buildFiles+'assets/images'));

	return merge(fonts,images);
});

gulp.task('browserify', ['views'], function() {
  return browserify(clientSourcePath+'app.js',{ debug: true })
      .transform(babelify)
//      .transform(ngAnnotate)
      .bundle()
      .on('error', interceptErrors)
      //Pass desired output filename to vinyl-source-stream
      .pipe(source('main.js'))
      // Start piping stream to tasks!
	  .pipe(buffer())
	  .pipe(sourcemaps.init({loadMaps: true}))
	  .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(buildFiles));
});

gulp.task('html', function() {
  return gulp.src([clientSourcePath+"index.html"])
      .on('error', interceptErrors)
      .pipe(gulp.dest(buildFiles));
});

gulp.task('views', function() {
  return gulp.src(viewFiles)
      .pipe(templateCache({
        standalone: true
      }))
      .on('error', interceptErrors)
      .pipe(rename("app.templates.js"))
      .pipe(gulp.dest(clientSourcePath));
});

// This task is used for building production ready
// minified JS/CSS files into the dist/ folder
gulp.task('build', ['html','css', 'browserify'], function() {
	var html = gulp.src(buildFiles+ 'index.html')
		.pipe(gulp.dest('./dist/'));

	var js = gulp.src(buildFiles+'main.js')
		.pipe(uglify())
		.pipe(gulp.dest('./dist/'));

	var css = gulp.src(buildFiles+'style.css')
		.pipe(cleanCSS())
		.pipe(gulp.dest('./dist/'));

	var fonts = gulp.src(buildFiles+'assets/fonts')
		.pipe(gulp.dest('./dist/'));

	var images = gulp.src(buildFiles+'assets/images')
		.pipe(gulp.dest('./dist/'));

	return merge(html,js,css,fonts,images);
});
gulp.task('servedist', function(){

	browserSync.init({
		server: "./dist"
	});

});
gulp.task('default', ['html', 'css', 'assets', 'browserify'], function() {

	// configure nodemon
	nodemon({
		// the script to run the app
		script: 'bin/server/index.js',
		// this listens to changes in any of these files/routes and restarts the application
		watch: ["bin/server/index.js"],
		ext: 'js'
		//
		// Below i'm using es6 arrow functions but you can
		// remove the arrow and have it a normal
		// .on('restart', function() { // then place your stuff in here }
		//
	}).on('restart', () => {
		// I've added notify, which displays a message on restart.
		// Was more for me to test so you can remove this
		//gulp.src('server.js').pipe(notify('Reloading Server JS'));
		console.log('Restarting ServerJS due to file change');
	});

  gulp.watch(clientSourcePath+"index.html", ['html']);
  gulp.watch(viewFiles, ['views']);
  gulp.watch(jsFiles, ['browserify']);
  gulp.watch(cssFiles, ['css']);

});

//
// PRODUCTION SERVER for HEROKU
//
gulp.task('webserver', () => {



});
