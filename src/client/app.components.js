import angular from 'angular';

import * as firebase from 'firebase/app';
//import 'firebase/database';
import 'angularfire';

let componentsModule = angular.module('app.components', ['firebase']);
console.log('components');

import PhonebookComponent from './phonebook/phonebook.component';
componentsModule.component('phonebook', PhonebookComponent);

export default componentsModule;