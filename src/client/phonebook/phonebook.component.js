//
// PHONEBOOK COMPONENT
//
// DATABASE: FIREBASE | PUBLIC
// API: ANGULARFIRE
//
import * as firebase from 'firebase/app';

class PhonebookController {

    constructor($scope, $firebaseArray, $mdDialog){

        'ngInject';

        this.scope = $scope;
        this.firebaseArray = $firebaseArray;


        this.scope.loader = true;

        this.getContacts();

        this.scope.addContact = () => {
            this.scope.contacts.$add(this.scope.new);
            $mdDialog.hide();
            console.log('added');
        }


        this.scope.editDetails = (id) => {

            // const el = document.querySelector('.edit_person'+id);
            this.addActionState = (this.addActionState == id) ? 0 : id;

            this.scope.edited = (this.addActionState == id) ? true : false;
        }

        this.scope.showEditPanel = (id, person) => {

            this.scope.edit = {
                name: person.name,
                phone: person.phone
            }
            
            const visibility = (id == this.addActionState) ? true : false;
            return visibility;

        }

        $scope.showAddPanel = (ev) => {
            this.addActionState = 0;

            $mdDialog.show({
                contentElement: '.add.md-dialog-container',
                parent: angular.element(document.body),
                preserveScope: false,
                clickOutsideToClose: true
                });
        };

        this.scope.removeContact = (ev, person) => {

            //
            // Appending dialog to document.body
            //
            var confirm = $mdDialog.confirm()
                    .title('Would you like to remove ' + person.name + ' from contacts?')
                    .textContent('WARNING: This action cannot be undone.')
                    .ariaLabel('Removing contact')
                    .targetEvent(ev)
                    .ok('CONFIRM')
                    .cancel('CANCEL');

            $mdDialog.show(confirm).then(() => {
                this.scope.contacts.$remove(person);
            }, ()=> {
                return false;
            });

        }

        this.scope.saveContact = (person) => {
            this.scope.contacts.$save(person);
            this.addActionState = 0;
        }


    }

    addContact(){

        //
        // ANGULAR-FIRE METHOD
        //
        this.scope.contacts.$add(this.scope.new);
        console.log('added');
    }

    getContacts(){

        this.scope.loaderMessage = "Retrieving data";

        var ref = firebase.database().ref().child("contacts");
        //
        // CREATE A SYNCHRONIZED ARRAY
        //
        const contactsPayload = this.firebaseArray(ref);
        contactsPayload.$loaded()
        .then(() => {
            this.scope.loader = false;
            _.orderBy(contactsPayload, ['name'], ['asc']);

            this.scope.contacts = contactsPayload;
        })
        .catch((error) => {
            this.scope.loaderMessage = "Error fetching data";
        });

    }

}

    let PhonebookComponent = {

        templateUrl: 'phonebook/phonebook.component.html',
        bindings: '=',
        controller: PhonebookController

    }
    export default PhonebookComponent;
