export const ADD_CONTACT = 'ADD_CONTACT'
export const REMOVE_CONTACT = 'REMOVE_CONTACT'

export function addContact(details) {
  return { type: ADD_CONTACT, details }
}

export function removeContact(id) {
  return { type: REMOVE_CONTACT, id }
}
