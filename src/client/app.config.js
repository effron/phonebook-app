import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

export default function appConfig($logProvider,$mdThemingProvider){

    'ngInject';
    //
    // Initialize Firebase
    //
    firebase.initializeApp({
        apiKey: "AIzaSyCnT4gcayjZ6bWo_aYnIEKHy6dqHi_E-O8",
        authDomain: "transferwise-phonebook-app.firebaseapp.com",
        databaseURL: "https://transferwise-phonebook-app.firebaseio.com",
        projectId: "transferwise-phonebook-app",
        storageBucket: "transferwise-phonebook-app.appspot.com",
        messagingSenderId: "410614359067"
    });

     // Configure a dark theme with primary foreground yellow

     $mdThemingProvider.theme('docs-dark', 'default')
     .primaryPalette('yellow')
     .dark();

}
