const AppConstants = {
    api: 'https://localhost:8000/api',
    appName: 'Phonebook App',
  };

  export default AppConstants;
