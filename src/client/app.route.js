//import * as AppActions from './actions';

function appRoute($routeProvider, $locationProvider){
    'ngInject';
    console.log('route');
	/*
	  If you don't want hashbang routing, uncomment this line.
	  Our tutorial will be using hashbang routing though :)
	*/
	// $locationProvider.html5Mode(true);

	$locationProvider.hashPrefix('!');

        //$locationProvider.html5Mode(true);
        $routeProvider
        .when('/', {
            templateUrl: 'views/phonebook.html'
        })
        .when('/phonebook', {
            templateUrl: 'views/phonebook.html'
        })
        .otherwise('/');//if no matches


}
export default appRoute;
