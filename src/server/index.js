//
// B A C K E N D _ S E T U P
//

//
// Init variables
//
const express  = require('express'),
	  app      = express(),
	  //
	  // process.env.PORT lets the port be set by Prod env
	  //
	  envPath = (app.settings.env == 'development') ? '/../client' : '/../../dist',
	  port = process.env.PORT || 8001,
	  appDir = __dirname + envPath,
	  isDev = (app.settings.env == 'development') ? true : false,
	  mime = require('mime'),
	  developer = {

	  	browserSync: require('browser-sync').create(),
	  	notifier: require('node-notifier'),

	  };


if(isDev){

	developer.browserSync.init(['bin/client/**/**.**'],{
		proxy: 'localhost:'+port,
		port: 8000,
		notify: true,
	});

}

var notify = (message) => {

	if(isDev){
		developer.notifier.notify({
			title: 'TRANSFERWISE NODE SERVER',
			message: message,
			icon: 'bin/client/favicon.png',
			sound: true,
			wait: true
		});
	}
}

notify('Server application initialized.');

//var redis = require('redis');
//var client = redis.createClient(); //creates a new client

import routes from './routes'
routes(app);

app.use(express.static(appDir));

app.use(function (req, res, next) {
	res.status(404);
	//this.type = mime.lookup(req.originalUrl);
	//console.log(this.type + ' ', Date.now())
	//res.type(this.type);

	res.status(404).send('ohh');

	//next()
})

//
// Listen (start app with node server.js) ======================================
//
app.listen(port);
console.log("Node APP listening in '"+appDir+"', on port "+ port);
